//
//  HeroViewModelSpec.swift
//  HeroesTests
//
//  Created by Aron Uchoa Bruno on 19/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import Quick
import Nimble
import Foundation

@testable import Heroes

class HeroViewModelSpec: QuickSpec {
    
    override func spec() {
        
        var sut: HeroViewModel!
        
        describe("HeroViewModel") {
            
            context("When heroViewModel is setup", {
                
                afterEach {
                    sut = nil
                }
                
                beforeEach {
                    
                    if let path = Bundle(for: type(of: self)
                        ).path(forResource: "hero", ofType: "json") {
                        do {
                            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                            let decoder = JSONDecoder()
                            decoder.keyDecodingStrategy = .convertFromSnakeCase
                            let serviceResult = try decoder.decode(ServiceResult.self, from: data)
                            sut = HeroViewModel(hero: serviceResult.data.results[0])
                        } catch {
                            fail("Problem parsing JSON")
                        }
                    }
                    
                }
                
                it("should have the correct data", closure: {
                    
                    expect(sut.id).to(equal(1011334))
                    expect(sut.name).to(equal("3-D Man"))
                    expect(sut.description).to(equal(""))
                    expect(sut.image).to(equal(
                        "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg"))
                })
                
            })
            
        }
    }
}
