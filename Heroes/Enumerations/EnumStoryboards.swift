//
//  EnumStoryboards.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 18/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import UIKit

enum EnumStoryboards:String {
    
    case Main
    case HeroDetail
    case Favorite
    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController(viewControllerClass:UIViewController.Type) -> UIViewController {
        return self.instance.instantiateViewController(withIdentifier: viewControllerClass.storyboardID)
    }
    
}
