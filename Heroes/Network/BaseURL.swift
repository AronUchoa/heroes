//
//  BaseURL.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 17/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import Foundation

public struct BaseURL {
    
    static let shared = BaseURL()
    
    public var url: URLComponents
    
    private init() {
        url = URLComponents()
        url.scheme = "https"
        url.host = "gateway.marvel.com"
    }
}
