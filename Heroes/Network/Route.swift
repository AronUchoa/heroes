//
//  Route.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 17/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import Foundation

enum Route: String {
    case heroes = "/v1/public/characters"
}
