//
//  UIImageView+Ext.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 18/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func setImage(url: String, placeHolder: UIImage) {
        let url = URL(string: url)
        let processor = DownsamplingImageProcessor(size: self.frame.size)
        self.kf.indicatorType = .activity
        self.kf.setImage(
            with: url,
            placeholder: placeHolder,
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
}
