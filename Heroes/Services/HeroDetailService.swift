//
//  HeroDetailService.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 18/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import Foundation
import Alamofire

class HeroDetailService {
    
    private let kTimeStamp = 1
    
    func getHeroDetail(id: Int, _ callBack: @escaping(ServiceResult?) -> Void) {
        
        let md5Data = HashMD5().getMD5(string: "\(kTimeStamp)\(Credentials.shared.privateKey)\(Credentials.shared.publicKey)")
        let md5Hex =  md5Data.map { String(format: "%02hhx", $0) }.joined()
        
        var components = BaseURL.shared.url
        components.path = Route.heroes.rawValue + "/\(id)"
        components.queryItems = [
            URLQueryItem(name: "apikey", value: Credentials.shared.publicKey),
            URLQueryItem(name: "hash", value: md5Hex),
            URLQueryItem(name: "ts", value: String(kTimeStamp))
        ]
        
        guard let url = components.url else {
            callBack(nil)
            return
        }
        
        Alamofire.request(url).validate().responseJSON { result in
            let decoder = JSONDecoder()
            
            guard let data = result.data else {
                callBack(nil)
                return
            }
            
            do {
                let response = try decoder
                    .decode(ServiceResult.self,
                            from: data)
                
                callBack(response)
                
            } catch {
                callBack(nil)
                print("caught: \(error)")
            }
        }
    }
}
