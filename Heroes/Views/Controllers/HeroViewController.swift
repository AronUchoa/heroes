//
//  HeroViewController.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 18/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import UIKit

class HeroViewController: UIViewController {

    @IBOutlet weak var heroTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let kBaselineFavoriteBorderBlack = "baseline_filter_list_black"
    private var kTitle = "Lista de Heróis"
    private var kRowHeight: CGFloat = 80.0
    private var heroViewModels = [HeroViewModel]()
    private var dataSource : HeroDataSource?
    private var offset = 0
    private var total = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        getHeroes()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
    }
    
    private func setup() {
        title = kTitle
        heroTableView.delegate = self
        navigationController?.navigationBar.tintColor = .black
        configureRightBarButton()
    }
    
    private func configureRightBarButton() {
        let favoriteButton = UIBarButtonItem(image: UIImage(named: kBaselineFavoriteBorderBlack), style: .plain, target: self, action: #selector(favoriteList))
        self.navigationItem.rightBarButtonItem  = favoriteButton
    }
    
    @objc private func favoriteList() {
        guard let destination = EnumStoryboards.Favorite.viewController(viewControllerClass: FavoriteHeroesViewController.self) as? FavoriteHeroesViewController else { return }
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    private func getHeroes() {
        activityIndicator.startAnimating()
        HeroService().getHeroes(offset: offset) { (response) in
            if response != nil {
                guard let data = response?.data else {
                    return
                }
                self.offset += data.count
                self.total = data.total
                let newHeroViewModels = data.results.map({return HeroViewModel(hero: $0)})
                self.setHeroes(newHeroViewModels)
            }
        }
    }
    
    private func setHeroes(_ heroes: [HeroViewModel]) {
        for hero in heroes {
            heroViewModels.append(hero)
        }
        self.dataSource = HeroDataSource(self.heroViewModels)
        heroTableView.dataSource = self.dataSource
        activityIndicator.stopAnimating()
        activityIndicator.hidesWhenStopped = true
        heroTableView.reloadData()
    }

}

extension HeroViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kRowHeight
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == heroViewModels.count - 5 {
            if heroViewModels.count < self.total {
                getHeroes()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let destination = EnumStoryboards.HeroDetail.viewController(viewControllerClass: HeroDetailViewController.self) as? HeroDetailViewController else { return }
        destination.heroID = heroViewModels[indexPath.row].id
        self.navigationController?.pushViewController(destination, animated: true)
    }
}
