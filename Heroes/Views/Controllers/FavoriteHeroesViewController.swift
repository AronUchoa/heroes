//
//  FavoriteHeroesViewController.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 19/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import UIKit

class FavoriteHeroesViewController: UIViewController {

    @IBOutlet weak var favoriteHeroesTableView: UITableView!
    
    private var kTitle = "Heróis Favoritos"
    private var kRowHeight: CGFloat = 80.0
    private var favoriteHeroesViewModels = [FavoriteHeroesViewModel]()
    private var dataSource : FavoriteHeroesDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        getFavoriteHeroes()
        updateFavoriteHeroes()
    }
    
    private func setup() {
        title = kTitle
        favoriteHeroesTableView.delegate = self
    }

    private func getFavoriteHeroes() {
        let heroes = CoreDataHelper.retrieveHeros()
        self.favoriteHeroesViewModels = heroes.map({return FavoriteHeroesViewModel(heroObject: $0)})
        self.dataSource = FavoriteHeroesDataSource(self.favoriteHeroesViewModels)
        favoriteHeroesTableView.dataSource = self.dataSource
        favoriteHeroesTableView.reloadData()
    }
    
    func updateFavoriteHeroes() {
        dataSource?.removeItem {
            self.getFavoriteHeroes()
            self.favoriteHeroesTableView.reloadData()
        }
    }
    
}

extension FavoriteHeroesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kRowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let destination = EnumStoryboards.HeroDetail.viewController(viewControllerClass: HeroDetailViewController.self) as? HeroDetailViewController else { return }
        destination.heroID = favoriteHeroesViewModels[indexPath.row].id
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
}
