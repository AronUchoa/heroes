//
//  HeroDetailViewController.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 19/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import UIKit

class HeroDetailViewController: UIViewController {
    
    private let kHeroDetailTitle = "Detalhe"
    var heroID = -1
    private let kBaselineFavoriteBorderBlack = "baseline_favorite_border_black"
    private let kBaselineFavoriteBlack = "baseline_favorite_black"
    private var heroViewModel : HeroViewModel!

    @IBOutlet weak var heroImage: UIImageView!
    @IBOutlet weak var heroName: UILabel!
    @IBOutlet weak var heroDescription: UITextView!    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        getHeroDetail()
    }
    
    private func setup() {
        title = kHeroDetailTitle
        configureRightBarButton()
    }
    
    private func configureRightBarButton() {
        
        var imageNameFavoriteButton = ""
        let heroObjects = CoreDataHelper.retrieveHeroById(heroID: String(heroID))
        
        (heroObjects != []) ? (imageNameFavoriteButton = kBaselineFavoriteBlack) : (imageNameFavoriteButton = kBaselineFavoriteBorderBlack)
        
        let favoriteButton = UIBarButtonItem(image: UIImage(named: imageNameFavoriteButton), style: .plain, target: self, action: #selector(favoriteHero))
        self.navigationItem.rightBarButtonItem  = favoriteButton
    }
    
    @objc
    private func favoriteHero() {
        
        let heroObjects = CoreDataHelper.retrieveHeroById(heroID: String(heroID))
        
        if heroObjects != [] {
            CoreDataHelper.delete(hero: heroObjects.first!)
        } else {
            CoreDataHelper.newHero(heroViewModel: self.heroViewModel)
        }
        configureRightBarButton()
    }
    
    private func getHeroDetail() {
        if heroID != -1 {
            HeroDetailService().getHeroDetail(id: heroID) { (response) in
                if response != nil {
                    guard let data = response?.data else {
                        return
                    }
                    self.heroViewModel = HeroViewModel(hero: data.results[0])
                    self.heroImage.setImage(url: self.heroViewModel.image, placeHolder: UIImage())
                    self.heroName.text = self.heroViewModel.name
                    self.heroDescription.text = self.heroViewModel.description
                }
            }
        }
    }

}
