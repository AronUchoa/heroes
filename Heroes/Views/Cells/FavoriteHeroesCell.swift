//
//  FavoriteHeroesCell.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 19/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import UIKit

class FavoriteHeroesCell: UITableViewCell {

    @IBOutlet weak var heroImage: UIImageView!
    @IBOutlet weak var heroName: UILabel!

}
