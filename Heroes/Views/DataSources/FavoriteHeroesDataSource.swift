//
//  FavoriteHeroesDataSource.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 19/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import UIKit
import Foundation

class FavoriteHeroesDataSource: NSObject, UITableViewDataSource {
    
    private let kCellIdentifier = "FavoriteHeroesCell"
    private var favoriteHeroesViewModels = [FavoriteHeroesViewModel]()
    
    private var removeItemCallBack : (()->Void)?
    
    func removeItem(callback: @escaping () -> Void) {
        removeItemCallBack = callback
    }
    
    init(_ favoriteHeroesViewModelArray: [FavoriteHeroesViewModel]) {
        self.favoriteHeroesViewModels = favoriteHeroesViewModelArray
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteHeroesViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kCellIdentifier) as! FavoriteHeroesCell
        let hero = favoriteHeroesViewModels[indexPath.row]
        cell.heroName.text = hero.name
        cell.heroImage.setImage(url: hero.image, placeHolder: UIImage())
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let heroObject = CoreDataHelper.retrieveHeroById(heroID: String(favoriteHeroesViewModels[indexPath.row].id))
            CoreDataHelper.delete(hero: heroObject.first!)
            if let callback = removeItemCallBack {
                callback()
            }
        }
    }
}
