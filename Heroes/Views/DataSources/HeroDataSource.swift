//
//  HeroDataSource.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 18/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import UIKit
import Foundation

class HeroDataSource: NSObject, UITableViewDataSource {
    
    private let kNumberOfSection = 1
    private let kCellIdentifier = "HeroCell"
    private var heroViewModelArray = [HeroViewModel]()
    private var fetchingMore = false
    
    init(_ heroViewModelArray: [HeroViewModel]) {
        self.heroViewModelArray = heroViewModelArray
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return kNumberOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return heroViewModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.kCellIdentifier) as! HeroCell
        let hero = heroViewModelArray[indexPath.row]
        cell.heroName.text = hero.name
        cell.heroImage.setImage(url: hero.image, placeHolder: UIImage())
        return cell
    }
}
