//
//  Hero.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 18/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import Foundation

struct Hero: Decodable {
    let id : Int
    let name : String
    let description : String
    let thumbnail : Thumbnail
    
    enum CodingKeys : String, CodingKey {
        case id
        case name
        case thumbnail
        case description
    }
}
