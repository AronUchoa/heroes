//
//  ServiceResult.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 18/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import Foundation

struct ServiceResult: Decodable {
    let code: Int
    let status : String
    let data : Content
    
    enum CodingKeys : String, CodingKey {
        case code
        case status
        case data
    }
}
