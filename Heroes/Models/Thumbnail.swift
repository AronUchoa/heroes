//
//  Thumbnail.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 18/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import Foundation

struct Thumbnail: Decodable {
    let path : String
    let imageExtension : String
    
    enum CodingKeys : String, CodingKey {
        case path
        case imageExtension = "extension"
    }
}
