//
//  Content.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 18/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import Foundation

struct Content: Decodable {
    let offset: Int
    let limit : Int
    let total : Int
    let count : Int
    let results : [Hero]
    
    enum CodingKeys : String, CodingKey {
        case offset
        case limit
        case total
        case count
        case results
    }
}
