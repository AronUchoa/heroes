//
//  HeroViewModel.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 18/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import Foundation

struct HeroViewModel {
    
    let id: Int
    let name: String
    let image: String
    let description: String
    
    init(hero: Hero) {
        self.id = hero.id
        self.name = hero.name
        self.image = "\(hero.thumbnail.path).\(hero.thumbnail.imageExtension)"
        self.description = hero.description
    }
}
