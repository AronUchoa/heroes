//
//  FavoriteHeroesViewModel.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 19/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import Foundation

struct FavoriteHeroesViewModel {
    
    let id: Int
    let name: String
    let image: String
    let description: String
    
    init(heroObject: HeroObject) {
        
        self.id = Int(heroObject.id!)!
        self.name = heroObject.name!
        self.image = heroObject.image!
        self.description = heroObject.heroDescription!
    }
}
