//
//  CoreDataHelper.swift
//  Heroes
//
//  Created by Aron Uchoa Bruno on 19/08/19.
//  Copyright © 2019 anonymous. All rights reserved.
//

import UIKit
import Foundation
import CoreData

struct CoreDataHelper {
    
    static let context: NSManagedObjectContext = {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError()
        }
        
        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext
        
        return context
    }()
    
    static func newHero(heroViewModel: HeroViewModel) {
        let heroObject = NSEntityDescription.insertNewObject(forEntityName: "HeroObject", into: context) as! HeroObject
        
        heroObject.id = String(heroViewModel.id)
        heroObject.name = heroViewModel.name
        heroObject.heroDescription = heroViewModel.description
        heroObject.image = heroViewModel.image
        
        saveHero()
    }
    
    static func saveHero() {
        do {
            try context.save()
        } catch let error {
            print("Could not save \(error.localizedDescription)")
        }
    }
    
    static func delete(hero: HeroObject) {
        context.delete(hero)
        
        saveHero()
    }
    
    static func retrieveHeros() -> [HeroObject] {
        do {
            let fetchRequest = NSFetchRequest<HeroObject>(entityName: "HeroObject")
            let results = try context.fetch(fetchRequest)
            
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            
            return []
        }
    }
    
    static func retrieveHeroById(heroID: String) -> [HeroObject] {
        do {
            let fetchRequest = NSFetchRequest<HeroObject>(entityName: "HeroObject")
            fetchRequest.predicate = NSPredicate(format: "id = %@", heroID)
            let heroObjects = try context.fetch(fetchRequest)
            if heroObjects != [] {
                return heroObjects
            }
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            
            return []
        }
        return []
    }
}
