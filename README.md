# Heroes

The project fits on the requirements:

```
Swift 5;
Target iOS 12;
Autolayout;
Codable;
CoreData;
UnitTest;
```

### Building

Just open Heroes.xcworkspace and run on Xcode

It doesn't need run the follow commands cause the frameworks already be on the repository

```
$pod install
```

### Dependencies

```
Alamofire, used for requests;
Kingfisher, used for downloading and caching image from the web;
Quick, used for the test;
Nimble, used for the test;
```

### Design pattern 

```
MVVM
```
